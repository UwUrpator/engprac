def map[T, U](l: List[T], fun: T => U): List[U] = l match {
    case List() => List()
    case (head::tail) => (fun(head))::((map(tail, fun)): List[U]) 
}
def f(x: Int): String = x.toString + ’0’
val l = List(1, 2, 3)
map(l, f) //List[String] = List(”10”, ”20”, ”30”)


def length(l: Any): Int = l match { 
    case List() => 0 
    case head::tail => length(tail) + 1 
}
val l = List(”1”, ”2”, ”3”)
length(l) //Int = 3


def sum(l: List[Int]): Int = l match { 
    case List() => 0 
    case head::tail => sum(tail) + head 
}
val l = List(1, 2, 3)
sum(l) //Int = 6


def concat(a: List[AnyVal], b: List[AnyVal]): List[AnyVal] = a match { 
    case List() => b match { 
        case List() => List() 
        case head::tail => head::concat(a, tail) 
    } 
    case head::tail => head::concat(tail, b) 
}
val a = List(1, 2, 3) val b = List(true, false)
concat(a, b) //List[AnyVal] = (1, 2, 3, true, false)


def flatten(l: List[List[AnyVal]]): List[AnyVal] = l match { 
    case List() => List() 
    case head::tail => concat(head, flatten(tail)) 
}
val l = List(List(1, 2, 3), List(true, false), List(34.3, 323.34))
flatten(l) //List(1, 2, 3, true, false, 34.3, 323.34)
